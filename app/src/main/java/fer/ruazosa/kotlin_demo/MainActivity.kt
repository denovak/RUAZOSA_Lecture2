package fer.ruazosa.kotlin_demo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myButton: Button = findViewById(R.id.myButton)
        val applicationContext = this.applicationContext

        /*
        myButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Toast.makeText(applicationContext, "Its toast!", Toast.LENGTH_SHORT).show();
        }

        })*/


        myButton.setOnClickListener({ v -> Toast.makeText(applicationContext, "" +
                "Its toast!", Toast.LENGTH_SHORT).show(); })

    }
}
